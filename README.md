# xPubExplore

An android application to view details of a blockchain Extended Public Key (xPub)

## Development

Written in Kotlin, this application is powered by the Blockchain.info api

## Libraries
- Dagger2
- RetroFit
- Gson
- RxJava2
- Mockito
- Espresso
- Junit

## Architecture
- MVP (with viewmodels)