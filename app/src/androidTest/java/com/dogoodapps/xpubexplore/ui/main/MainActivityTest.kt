package com.dogoodapps.xpubexplore.ui.main

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.intent.Intents
import android.support.test.espresso.intent.Intents.intended
import android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.espresso.matcher.ViewMatchers.withText
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.dogoodapps.xpubexplore.R
import com.dogoodapps.xpubexplore.ui.wallet.WalletActivity
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    /*
     * This is a simple example of some UI testing.. interacting with views and checking
     * that the correct intent is being launched.. this would usually be a little more extensive
      * and test success and error scenarios
     */

    @Rule
    @JvmField
    var mActivityRule = ActivityTestRule(MainActivity::class.java)

    @Before
    fun setup() {
        Intents.init()
    }

    @Test
    fun clickDoneOnSoftKeyboard_whenValidAddressEntered_launchesWalletActivity() {
        onView(withId(R.id.xpubEditText))
                .perform(typeText("non_empty_string"), pressImeActionButton())
        intended(hasComponent(WalletActivity::class.java.name))
    }

    @Test
    fun clickGoButton_whenValidAddressEntered_launchesWalletActivity() {
        onView(withId(R.id.xpubEditText)).perform(typeText("non_empty_string"))
        onView(withId(R.id.xpubEditText)).perform(closeSoftKeyboard())
        onView(withId(R.id.goButton)).perform(click())
        intended(hasComponent(WalletActivity::class.java.name))
    }

    @Test
    fun clickLogo_populatesEditTextWithDemoXpub() {
        onView(withId(R.id.logoImage)).perform(click())
        onView(withId(R.id.xpubEditText)).check(matches(withText(MainPresenter.DEMO_XPUB)))
    }

    @After
    fun tearDown() {
        Intents.release()
    }
}