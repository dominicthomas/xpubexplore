package com.dogoodapps.xpubexplore.app


enum class Error {
    API, CONNECTIVITY, NONE
}