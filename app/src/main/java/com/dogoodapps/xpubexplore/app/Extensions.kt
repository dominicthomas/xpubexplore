package com.dogoodapps.xpubexplore.app

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.DrawableCompat
import android.view.Menu
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

/*
 * Ideally these would all be tested.. extension functions are awesome! but I'd likely prefer to
 * make them a little more re-usable and perhaps organise them into specific files/classes rather
 * than dumping all here.. for now it's useful to see them in one places
 */

// TODO: Implement with BigDecimal for better precision
fun Int.satoshiToBTC(): String {
    return DecimalFormat("0.00000000").format((this / 1e8))
}

// TODO: Implement with BigDecimal for better precision
fun Double.formatToTwoDecimalPlaces(): String {
    return DecimalFormat("0.00").format(this)
}

fun Menu.tintAllItems(color: Int) {
    (0 until this.size())
            .map { this.getItem(it) }
            .forEach {
                val drawable = it.icon
                val wrapped = DrawableCompat.wrap(drawable)
                drawable.mutate()
                DrawableCompat.setTint(wrapped, color)
                it.icon = drawable
            }
}


fun Date.fromUnixTimeStamp(unixTimeStamp: Int): Date {
    return Date(unixTimeStamp * 1000L)
}

fun Date.formatWith(sDF: SimpleDateFormat): String {
    return sDF.format(this)
}

fun TextView.onDonePressed(action: TextView.() -> Unit) {
    this.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, _ ->
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            val imm = v?.context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(v.windowToken, 0)
            action()
            return@OnEditorActionListener true
        }
        false
    })
}

fun TextView.clear() {
    this.text = null
}

fun Context.getCompatColor(colorResId: Int): Int {
    return ContextCompat.getColor(this, colorResId)
}

fun Context.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}