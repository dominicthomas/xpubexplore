package com.dogoodapps.xpubexplore.mvp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.dogoodapps.xpubexplore.App
import com.dogoodapps.xpubexplore.app.Error
import com.dogoodapps.xpubexplore.di.ActivityModule
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

interface MvpView {
    fun showLoading(visible: Boolean){}

    fun showError(error: Error){}
}

interface MvpPresenter<in T : MvpView> {
    fun attachView(view: T)

    fun detachView()
}

abstract class BaseMvpActivity<in V : MvpView, P : MvpPresenter<V>> : AppCompatActivity(), MvpView {

    val component by lazy { App.appComponent.attach(ActivityModule()) }

    @Inject
    lateinit var presenter: P

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()
        setContentView(getLayoutId())
        presenter.attachView(this as V)
    }

    abstract fun inject()

    abstract fun getLayoutId(): Int

    override fun onStop() {
        super.onStop()
        presenter.detachView()
    }
}


abstract class BaseMvpPresenter<V : MvpView> : MvpPresenter<V> {

    private val disposables: CompositeDisposable = CompositeDisposable()

    lateinit var view: V

    override fun attachView(view: V) {
        this.view = view
        init()
    }

    fun addDisposable(disposable: Disposable) {
        disposables.add(disposable)
    }

    override fun detachView() {
        disposables.dispose()
    }

    open fun init() {
        // No op
    }
}
