package com.dogoodapps.xpubexplore.di

import com.dogoodapps.xpubexplore.ui.main.MainActivity
import com.dogoodapps.xpubexplore.ui.wallet.WalletActivity
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = arrayOf(ActivityModule::class))
interface ActivityComponent {

    fun inject(mainActivity: MainActivity)

    fun inject(walletActivity: WalletActivity)
}