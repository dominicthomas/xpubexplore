package com.dogoodapps.xpubexplore.di

import com.dogoodapps.xpubexplore.App
import com.dogoodapps.xpubexplore.network.BlockChainApiService
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class, NetworkModule::class))
interface AppComponent {

    fun apiService(): BlockChainApiService

    fun inject(application: App)

    fun attach(activityModule: ActivityModule): ActivityComponent
}