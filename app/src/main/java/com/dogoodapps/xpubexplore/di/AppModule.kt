package com.dogoodapps.xpubexplore.di

import android.content.Context
import android.content.res.Resources
import com.dogoodapps.xpubexplore.App
import com.dogoodapps.xpubexplore.utils.ValidationManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val app: App) {

    @Provides
    @Singleton
    fun provideApp(): App = app

    @Provides
    @Singleton
    fun provideContext(): Context = app

    @Provides
    @Singleton
    fun provideValidationManager(): ValidationManager = ValidationManager()

    @Provides
    @Singleton
    fun provideResource(): Resources = app.resources

}
