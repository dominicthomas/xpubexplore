package com.dogoodapps.xpubexplore.utils

import javax.inject.Inject

open class ValidationManager @Inject constructor() {

    /**
     * I was trying to check the xpub address is valid using Base58::decodeChecked from the
     * bitcoinj library but has some problems compiling and needed to focus on other parts of the app
     */
    fun validateAddress(address: String): Boolean {
        // TODO: Implement proper xpub address validation
        return address.isNotEmpty() && address.isNotBlank()
    }
}