package com.dogoodapps.xpubexplore.network

import com.dogoodapps.xpubexplore.model.multiaddr.MultiAddr
import com.dogoodapps.xpubexplore.model.ticker.Ticker
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface BlockChainApiService {

    @GET("multiaddr")
    fun multiAddr(@Query("active") action: String): Single<MultiAddr>

    @GET("ticker")
    fun ticker(): Single<Ticker>
}