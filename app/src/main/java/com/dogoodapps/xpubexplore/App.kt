package com.dogoodapps.xpubexplore

import android.app.Application
import com.dogoodapps.xpubexplore.di.AppComponent
import com.dogoodapps.xpubexplore.di.AppModule
import com.dogoodapps.xpubexplore.di.DaggerAppComponent
import com.dogoodapps.xpubexplore.di.NetworkModule

class App : Application() {

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .networkModule(NetworkModule())
                .build()
        appComponent.inject(this)
    }
}
