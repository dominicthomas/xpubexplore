package com.dogoodapps.xpubexplore.model.multiaddr

import java.io.Serializable


data class Address(
        val address: String, //xpub6CfLQa8fLgtouvLxrb8EtvjbXfoC1yqzH6YbTJw4dP7srt523AhcMV8Uh4K3TWSHz9oDWmn9MuJogzdGU3ncxkBsAC9wFBLmFrWT9Ek81kQ
        val n_tx: Int, //305
        val total_received: Int, //27468351
        val total_sent: Int, //26351245
        val final_balance: Int, //1117106
        val gap_limit: Int, //20
        val change_index: Int, //156
        val account_index: Int //125
)

data class Info(
        val nconnected: Int, //0
        val conversion: Double, //100000000.00000000
        val symbol_local: SymbolLocal,
        val symbol_btc: SymbolBtc,
        val latest_block: LatestBlock
)

data class Input(
        val prev_out: PrevOut,
        val sequence: Long, //4294967295
        val script: String, //473044022037bd8b2115b2fb6b93b7d147ccb3eae333e8be7c49a4dd4a261c99950b5a8d3b022042bae62654b65c2e33520e9f37a1edfe045668f3ec73e899acc5548b8fbfbf32012103d17f27486951efff87d79dee99afddf1db0aab422d72b35df84631217e191c7a
        val witness: String
) : Serializable

data class LatestBlock(
        val block_index: Int, //1670452
        val hash: String, //0000000000000000000e7b6f2c27aadec0a99dd6e82a548f90eb4080439f481a
        val height: Int, //507315
        val time: Int //1517598609
)

data class MultiAddr(
        val recommend_include_fee: Boolean, //true
        val info: Info,
        val wallet: Wallet,
        val addresses: List<Address>,
        val txs: List<Tx>
)

data class Out(
        val value: Int, //15228
        val tx_index: Int, //326827134
        val n: Int, //0
        val spent: Boolean, //false
        val script: String, //76a9146b62594f29e0844e7a6b78dd0ef6e1318aef960c88ac
        val type: Int, //0
        val addr: String, //1Ano7vdpAYSqV8gWrtA5CkD35dKstYi2pV
        val xpub: Xpub
) : Serializable

data class PrevOut(
        val value: Int, //148904
        val tx_index: Int, //309319210
        val n: Int, //1
        val spent: Boolean, //true
        val script: String, //76a9141c03ddd4695c2e4d1a3483a6eea7c31c8173325f88ac
        val type: Int, //0
        val addr: String, //13Z8Z2GoFSJEGExF2L145VPtSB2BAoHABP
        val xpub: Xpub
)

data class SymbolBtc(
        val code: String, //BTC
        val symbol: String, //BTC
        val name: String, //Bitcoin
        val conversion: Double, //100000000.00000000
        val symbolAppearsAfter: Boolean, //true
        val local: Boolean //false
) : Serializable

data class SymbolLocal(
        val code: String, //USD
        val symbol: String, //$
        val name: String, //U.S. dollar
        val conversion: Double, //11743.52608766
        val symbolAppearsAfter: Boolean, //false
        val local: Boolean //true
)

data class Tx(
        val hash: String, //78414cbfee60b49c3909cfc26631dfd35002ec6130230868490edde546a1fc62
        val ver: Int, //1
        val vin_sz: Int, //1
        val vout_sz: Int, //2
        val size: Int, //225
        val weight: Int, //900
        val fee: Int, //5876
        val relayed_by: String, //127.0.0.1
        val lock_time: Int, //0
        val tx_index: Int, //326827134
        val double_spend: Boolean, //false
        val result: Int, //-133676
        val balance: Int, //1117106
        val time: Int, //1516885547
        val block_height: Int, //506051
        val inputs: List<Input>,
        val out: List<Out>
)

data class Wallet(
        val n_tx: Int, //305
        val n_tx_filtered: Int, //305
        val total_received: Int, //27468351
        val total_sent: Int, //26351245
        val final_balance: Int //1117106
)

data class Xpub(
        val m: String, //xpub6CfLQa8fLgtouvLxrb8EtvjbXfoC1yqzH6YbTJw4dP7srt523AhcMV8Uh4K3TWSHz9oDWmn9MuJogzdGU3ncxkBsAC9wFBLmFrWT9Ek81kQ
        val path: String //M/1/154
)