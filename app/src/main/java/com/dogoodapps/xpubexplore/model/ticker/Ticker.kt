package com.dogoodapps.xpubexplore.model.ticker

import com.google.gson.annotations.SerializedName


data class Ticker(
        val USD: Currency,
        val GBP: Currency
)

data class Currency(
        @SerializedName("15m") val fifteenM: Double, //8669.91
        val last: Double, //8669.91
        val buy: Double, //8672.91
        val sell: Double, //8666.91
        val symbol: String //$
)
