package com.dogoodapps.xpubexplore.ui.wallet.viewmodel

import com.dogoodapps.xpubexplore.R
import com.dogoodapps.xpubexplore.app.formatWith
import com.dogoodapps.xpubexplore.app.fromUnixTimeStamp
import com.dogoodapps.xpubexplore.app.satoshiToBTC
import com.dogoodapps.xpubexplore.model.multiaddr.Input
import com.dogoodapps.xpubexplore.model.multiaddr.Out
import com.dogoodapps.xpubexplore.model.multiaddr.SymbolBtc
import com.dogoodapps.xpubexplore.model.multiaddr.Tx
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.sign

class TxViewModel(transactionHash: String,
                  transactionResult: Int,
                  transactionTime: Int,
                  transactionFee: Int,
                  symbolBtc: SymbolBtc,
                  inputs: List<Input>,
                  out: List<Out>) : Serializable {

    companion object {

        // TODO: Move to a date util/extension
        private const val DATE_PATTERN = "MMM d, yyyy"
        private const val TIME_PATTERN = "HH:mm a"
        private const val POSITIVE_SYMBOL = "+"

        fun fromModels(tx: List<Tx>, symbolBtc: SymbolBtc): List<TxViewModel> {
            return tx.map { it }
                    .map { TxViewModel(it.hash, it.result, it.time, it.fee, symbolBtc, it.inputs, it.out) }
        }
    }

    private val resultInBtc = transactionResult.satoshiToBTC()

    val feeInBtc = transactionFee.satoshiToBTC()

    private val isReceived = resultInBtc.toDouble().sign > 0

    private val date = Date().fromUnixTimeStamp(transactionTime)

    private val sign = if (isReceived) POSITIVE_SYMBOL else ""

    val icon = when (isReceived) {
        true -> R.drawable.ic_tx_received
        false -> R.drawable.ic_tx_sent
    }

    val color = when (isReceived) {
        true -> R.color.colorReceived
        false -> R.color.colorSent
    }

    val string = when (isReceived) {
        true -> R.string.received
        false -> R.string.sent
    }

    // TODO: Create date formatting utils/extensions
    val formattedDate = date.formatWith(SimpleDateFormat(DATE_PATTERN, Locale.getDefault()))

    val formattedTime = date.formatWith(SimpleDateFormat(TIME_PATTERN, Locale.getDefault()))

    val signedResult = sign + resultInBtc + " " + symbolBtc.symbol
}