package com.dogoodapps.xpubexplore.ui.wallet

import com.dogoodapps.xpubexplore.mvp.MvpView
import com.dogoodapps.xpubexplore.ui.wallet.viewmodel.AddressViewModel
import com.dogoodapps.xpubexplore.ui.wallet.viewmodel.TxViewModel
import com.dogoodapps.xpubexplore.ui.wallet.viewmodel.WalletViewModel

interface WalletView : MvpView {

    fun showWallet(wallet: WalletViewModel)

    fun showAddressDialog(addressViewModel: AddressViewModel)

    fun showTxInfoDialog(txViewModel: TxViewModel)
}
