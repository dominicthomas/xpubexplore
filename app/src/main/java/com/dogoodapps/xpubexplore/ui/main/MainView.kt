package com.dogoodapps.xpubexplore.ui.main

import com.dogoodapps.xpubexplore.mvp.MvpView

interface MainView : MvpView {

    fun loadWallet(xpub: String)

    fun showErrorMessage(errorMessage: String)

    fun setDemoXpub(demoXpub: String)
}