package com.dogoodapps.xpubexplore.ui.wallet

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.dogoodapps.xpubexplore.R
import com.dogoodapps.xpubexplore.app.Error
import com.dogoodapps.xpubexplore.app.tintAllItems
import com.dogoodapps.xpubexplore.mvp.BaseMvpActivity
import com.dogoodapps.xpubexplore.ui.adapters.TxAdapter
import com.dogoodapps.xpubexplore.ui.dialog.WalletDialogFragment
import com.dogoodapps.xpubexplore.ui.wallet.viewmodel.AddressViewModel
import com.dogoodapps.xpubexplore.ui.wallet.viewmodel.TxViewModel
import com.dogoodapps.xpubexplore.ui.wallet.viewmodel.WalletViewModel
import kotlinx.android.synthetic.main.activity_wallet.*


class WalletActivity : BaseMvpActivity<WalletView, WalletPresenter>(), WalletView {

    companion object {
        private val KEY_EXTRA_XPUB = "KEY_EXTRA_XPUB"

        fun newIntent(context: Context, xpub: String): Intent {
            val intent = Intent(context, WalletActivity::class.java)
            intent.putExtra(KEY_EXTRA_XPUB, xpub)
            return intent
        }
    }

    private lateinit var txAdapter: TxAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initToolbar()
        initViews()
    }

    private fun initToolbar() {
        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
    }

    private fun initViews() {
        txAdapter = TxAdapter(mutableListOf(), presenter)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = txAdapter
        // TODO: Add a custom scroll listener/adapter to load more (paging)
    }

    override fun inject() {
        component.inject(this)
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_wallet
    }

    override fun onStart() {
        super.onStart()
        val xpub = intent.extras.get(KEY_EXTRA_XPUB) as String?
        xpub?.let {
            presenter.loadData(xpub)
        }
    }

    override fun onStop() {
        super.onStop()
        presenter.detachView()
    }

    override fun showWallet(wallet: WalletViewModel) {
        finalBalance.text = wallet.formattedFinalBalance
        fiatBalance.text = wallet.convertBalanceTo(wallet.usd)
        txAdapter.update(wallet.txs)
    }

    override fun showLoading(visible: Boolean) {
        walletLoadingSpinner.visibility = when (visible) {
            true -> View.VISIBLE
            false -> View.GONE
        }
    }

    override fun showError(error: Error) {
        when (error) {
            Error.API -> {
                errorImage.setImageResource(R.drawable.ic_cloud_off)
                errorMessage.text = getString(R.string.api_error_message)
            }
            Error.CONNECTIVITY -> {
                showLoading(false)
                errorImage.setImageResource(R.drawable.ic_no_internet)
                errorMessage.text = getString(R.string.no_internet_error_message)
            }
            else -> {
                showLoading(false)
                errorImage.setImageResource(R.drawable.ic_unknown_error)
                errorMessage.text = getString(R.string.unknown_error_message)
            }
        }
    }

    override fun showAddressDialog(addressViewModel: AddressViewModel) {
        // I would've liked to display a nice custom view to show the details of the current address
        val newInstance = WalletDialogFragment.newInstance(addressViewModel)
        newInstance.show(this.supportFragmentManager, "address")
    }

    override fun showTxInfoDialog(txViewModel: TxViewModel) {
        // Same here
        val newInstance = WalletDialogFragment.newInstance(txViewModel)
        newInstance.show(this.supportFragmentManager, "transaction")
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)
        menu?.tintAllItems(ContextCompat.getColor(this, android.R.color.white))
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.action_info -> presenter.onInfoMenuItemClicked()
            else -> super.onOptionsItemSelected(item)
        }
    }
}