package com.dogoodapps.xpubexplore.ui.wallet

import com.dogoodapps.xpubexplore.model.multiaddr.MultiAddr
import com.dogoodapps.xpubexplore.model.ticker.Ticker
import com.dogoodapps.xpubexplore.network.BlockChainApiService
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class WalletDataManager @Inject constructor(
        private val blockChainApiService: BlockChainApiService) {

    private var multiAddr: MultiAddr? = null

    fun multiAddr(xpub: String): Single<MultiAddr> {
        return blockChainApiService.multiAddr(xpub)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun ticker(): Single<Ticker> {
        return blockChainApiService.ticker()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    /**
     * Ideally this would be persisted so the data can be accessed after
     * the api call.. the data store would be refreshed when required.. storing in memory for now
     */
    fun saveData(multiaddr: MultiAddr?) {
        this.multiAddr = multiaddr
    }

    fun getData(): MultiAddr? {
        return multiAddr
    }
}