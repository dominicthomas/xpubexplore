package com.dogoodapps.xpubexplore.ui.wallet

import com.dogoodapps.xpubexplore.app.Error
import com.dogoodapps.xpubexplore.model.multiaddr.MultiAddr
import com.dogoodapps.xpubexplore.model.ticker.Ticker
import com.dogoodapps.xpubexplore.mvp.BaseMvpPresenter
import com.dogoodapps.xpubexplore.ui.adapters.TxAdapter
import com.dogoodapps.xpubexplore.ui.wallet.viewmodel.AddressViewModel
import com.dogoodapps.xpubexplore.ui.wallet.viewmodel.TxViewModel
import com.dogoodapps.xpubexplore.ui.wallet.viewmodel.WalletViewModel
import com.dogoodapps.xpubexplore.utils.NetworkHelper
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import javax.inject.Inject

class WalletPresenter @Inject constructor(
        private val walletDataManager: WalletDataManager,
        private val networkHelper: NetworkHelper)
    : BaseMvpPresenter<WalletView>(), TxAdapter.OnTxItemClickedListener {

    fun loadData(xpub: String) {
        when (networkHelper.isNetworkConnected()) {
            true -> Single.zip(walletDataManager.ticker(), walletDataManager.multiAddr(xpub),
                    BiFunction<Ticker, MultiAddr, WalletViewModel> { ticker, multiaddr ->
                        walletDataManager.saveData(multiaddr)
                        WalletViewModel.fromModel(ticker, multiaddr)
                    })
                    .doOnSubscribe({ view.showLoading(true) })
                    .doFinally({ view.showLoading(false) })
                    .subscribe(
                            { walletViewModel ->
                                view.showWallet(walletViewModel)
                            },
                            { error: Throwable? ->
                                view.showError(Error.API)
                            })
            false -> view.showError(Error.CONNECTIVITY)
        }
    }

    fun onInfoMenuItemClicked(): Boolean {
        walletDataManager.getData()?.let { multiAddr ->
            val address = multiAddr.addresses[0]
            view.showAddressDialog(AddressViewModel.fromModel(address))
            return true
        }
        return false
    }

    override fun onTxItemClicked(txViewModel: TxViewModel) {
        view.showTxInfoDialog(txViewModel)
    }
}