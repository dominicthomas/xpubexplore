package com.dogoodapps.xpubexplore.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dogoodapps.xpubexplore.R
import com.dogoodapps.xpubexplore.app.getCompatColor
import com.dogoodapps.xpubexplore.ui.wallet.viewmodel.TxViewModel
import kotlinx.android.synthetic.main.layout_transaction_item.view.*

class TxAdapter(private val transactions: MutableList<TxViewModel>,
                private val onClickListener: OnTxItemClickedListener)
    : RecyclerView.Adapter<TxAdapter.ViewHolder>() {

    interface OnTxItemClickedListener {
        fun onTxItemClicked(txViewModel: TxViewModel)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent?.context)
                .inflate(R.layout.layout_transaction_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return transactions.size
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.bind(transactions[position], onClickListener)
    }

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

        fun bind(txViewModel: TxViewModel?, clickListener: OnTxItemClickedListener) {
            txViewModel?.let {
                // TODO: Add hash, fee, in/out to layout and onclick to show all additional info!
                val color = itemView.context.getCompatColor(it.color)
                itemView.actionIcon.setBackgroundResource(it.icon)
                itemView.actionText.text = itemView.context.getString(it.string).toUpperCase()
                itemView.actionText.setTextColor(color)
                itemView.dateText.text = it.formattedDate
                itemView.timeText.text = it.formattedTime
                itemView.resultText.text = it.signedResult
                itemView.resultText.setTextColor(color)
                itemView.setOnClickListener { clickListener.onTxItemClicked(txViewModel) }
            }
        }
    }

    fun update(txs: List<TxViewModel>) {
        transactions.clear()
        transactions.addAll(txs)
        notifyDataSetChanged()
    }
}