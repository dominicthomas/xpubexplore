package com.dogoodapps.xpubexplore.ui.main

import android.os.Bundle
import android.widget.Toast
import com.dogoodapps.xpubexplore.R
import com.dogoodapps.xpubexplore.app.clear
import com.dogoodapps.xpubexplore.app.onDonePressed
import com.dogoodapps.xpubexplore.app.toast
import com.dogoodapps.xpubexplore.mvp.BaseMvpActivity
import com.dogoodapps.xpubexplore.ui.wallet.WalletActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : BaseMvpActivity<MainView, MainPresenter>(), MainView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Ideally this call should support multiple addresses.. one should be enough for a demo
        xpubEditText.onDonePressed { presenter.onXPubEntered(this.text.toString()) }
        goButton.setOnClickListener({ presenter.onXPubEntered(xpubEditText.text.toString()) })

        // This is for demo purposes only.. will be removed later to protect the demo xpub
        logoImage.setOnClickListener { setDemoXpub(MainPresenter.DEMO_XPUB) }
        toast(getString(R.string.demo_message))
    }

    override fun inject() {
        component.inject(this)
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }

    override fun setDemoXpub(demoXpub: String) {
        xpubEditText.setText(demoXpub)
    }

    override fun loadWallet(xpub: String) {
        startActivity(WalletActivity.newIntent(this, xpub))
    }

    override fun showErrorMessage(errorMessage: String) {
        xpubEditText.clear()
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show()
    }
}
