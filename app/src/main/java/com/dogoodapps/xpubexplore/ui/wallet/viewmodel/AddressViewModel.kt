package com.dogoodapps.xpubexplore.ui.wallet.viewmodel

import com.dogoodapps.xpubexplore.model.multiaddr.Address
import java.io.Serializable

// TODO: Use this to populate the address info view.. WIP
class AddressViewModel(val address: String,
                       val numberOfTransaction: Int,
                       val totalReceived: Int,
                       val totalSent: Int,
                       val finalBalance: Int) : Serializable {

    companion object {
        fun fromModel(address: Address): AddressViewModel {
            return AddressViewModel(address.address, address.n_tx, address.total_received,
                    address.total_sent, address.final_balance)
        }
    }
}