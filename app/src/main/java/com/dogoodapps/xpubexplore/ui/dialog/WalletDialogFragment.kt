package com.dogoodapps.xpubexplore.ui.dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.dogoodapps.xpubexplore.R
import com.dogoodapps.xpubexplore.ui.wallet.viewmodel.AddressViewModel
import com.dogoodapps.xpubexplore.ui.wallet.viewmodel.TxViewModel

/**
 * Ideally this fragment would be more generic and could be extended to support different view
 * models and behaviours
 */
class WalletDialogFragment : DialogFragment() {

    companion object {

        private const val KEY_EXTRA_WALLET_VIEWMODEL = "KEY_EXTRA_WALLET_VIEWMODEL"

        private const val KEY_EXTRA_TX_VIEWMODEL = "KEY_EXTRA_TX_VIEWMODEL"

        fun newInstance(addressViewModel: AddressViewModel): WalletDialogFragment {
            val walletDialogFragment = WalletDialogFragment()
            val bundle = Bundle()
            bundle.putSerializable(KEY_EXTRA_WALLET_VIEWMODEL, addressViewModel)
            walletDialogFragment.arguments = bundle
            return walletDialogFragment
        }

        fun newInstance(txViewModel: TxViewModel): WalletDialogFragment {
            val walletDialogFragment = WalletDialogFragment()
            val bundle = Bundle()
            bundle.putSerializable(KEY_EXTRA_TX_VIEWMODEL, txViewModel)
            walletDialogFragment.arguments = bundle
            return walletDialogFragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.layout_wallet_dialog, container, false)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window.requestFeature(Window.FEATURE_NO_TITLE)
        // TODO: Inflate relevant layout and bind relevant views
        return view
    }
}