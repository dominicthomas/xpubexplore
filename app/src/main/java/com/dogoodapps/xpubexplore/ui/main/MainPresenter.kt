package com.dogoodapps.xpubexplore.ui.main

import android.content.res.Resources
import com.dogoodapps.xpubexplore.R
import com.dogoodapps.xpubexplore.mvp.BaseMvpPresenter
import com.dogoodapps.xpubexplore.utils.ValidationManager
import javax.inject.Inject

class MainPresenter @Inject constructor(
        private val validationManager: ValidationManager,
        private val resources: Resources) : BaseMvpPresenter<MainView>() {

    /**
     * Usually we would not want this extended public key exposed in the source code.. I left it here
     * for the demo to have something to load.. also validation isn't working yet
     */
    companion object {
        const val DEMO_XPUB = "xpub6CfLQa8fLgtouvLxrb8Et" +
                "vjbXfoC1yqzH6YbTJw4dP7srt523AhcMV8Uh4K3T" +
                "WSHz9oDWmn9MuJogzdGU3ncxkBsAC9wFBLmFrWT9Ek81kQ"
    }

    fun onXPubEntered(xpub: String) {
        if (validationManager.validateAddress(xpub)) {
            view.loadWallet(xpub)
        } else {
            view.showErrorMessage(resources.getString(R.string.invalid_xpub_entered))
        }
    }
}