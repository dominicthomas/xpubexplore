package com.dogoodapps.xpubexplore.ui.wallet.viewmodel

import com.dogoodapps.xpubexplore.app.formatToTwoDecimalPlaces
import com.dogoodapps.xpubexplore.app.satoshiToBTC
import com.dogoodapps.xpubexplore.model.multiaddr.MultiAddr
import com.dogoodapps.xpubexplore.model.multiaddr.SymbolBtc
import com.dogoodapps.xpubexplore.model.multiaddr.SymbolLocal
import com.dogoodapps.xpubexplore.model.ticker.Currency
import com.dogoodapps.xpubexplore.model.ticker.Ticker

data class WalletViewModel(val address: String,
                           val numberOfTransactions: Int,
                           val totalReceived: Int,
                           val totalSent: Int,
                           val finalBalance: Int,
                           val symbolBtc: SymbolBtc,
                           val symbolLocal: SymbolLocal,
                           val txs: List<TxViewModel>,
                           val usd: Currency) {

    companion object {
        /**
         * Handy place to get any required data from the provided data class.
         *
         * Just using usd from the ticker for now.. ideally the ticker would be managed separately
         * and we could observe events when a user chooses a different currency and perhaps
         * check the global settings or locale when loading this screen and choose the currency to
         * convert to accordingly
         */
        fun fromModel(ticker: Ticker, multiAddr: MultiAddr): WalletViewModel {
            val address = multiAddr.addresses[0].address // Currently only supporting the first address
            val symbolBtc = multiAddr.info.symbol_btc
            val symbolLocal = multiAddr.info.symbol_local
            val txViewmodels = TxViewModel.fromModels(multiAddr.txs, symbolBtc)
            val wallet = multiAddr.wallet
            return WalletViewModel(address, wallet.n_tx, wallet.total_received, wallet.total_sent,
                    wallet.final_balance, symbolBtc, symbolLocal, txViewmodels, ticker.USD)
        }
    }

    val formattedFinalBalance = finalBalance.satoshiToBTC() + " " + symbolBtc.symbol

    fun convertBalanceTo(currency: Currency): String {
        val formatToTwoDecimalPlaces = (finalBalance.satoshiToBTC().toDouble()
                * currency.fifteenM).formatToTwoDecimalPlaces()
        return (currency.symbol + formatToTwoDecimalPlaces)
    }
}

