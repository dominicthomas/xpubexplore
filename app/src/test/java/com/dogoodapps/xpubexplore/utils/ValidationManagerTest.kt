package com.dogoodapps.xpubexplore.utils

import junit.framework.Assert.assertFalse
import junit.framework.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class ValidationManagerTest {

    // This is just a string for now.. didn't get a chance to do proper validation
    val validXpub = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"

    val badXpub = ""

    private lateinit var validationManager: ValidationManager

    @Before
    fun setup() {
        validationManager = ValidationManager()
    }

    @Test
    fun validateAddress_whenXpubIsValid_returnsTrue() {
        assertTrue(validationManager.validateAddress(validXpub))
    }

    @Test
    fun validateAddress_whenXpubIsNotValid_returnsFalse() {
        assertFalse(validationManager.validateAddress(badXpub))
    }
}
