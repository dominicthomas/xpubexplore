package com.dogoodapps.xpubexplore.ui.main

import android.content.res.Resources
import com.dogoodapps.xpubexplore.utils.ValidationManager
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MainPresenterTest {

    /*
     * This is a simple example of testing the business logic in presenters..
     */

    @Mock
    lateinit var validationManager: ValidationManager

    @Mock
    lateinit var resources: Resources

    @Mock
    lateinit var mainView: MainView

    lateinit var mainPresenter: MainPresenter

    @Before
    fun setup() {
        mainPresenter = MainPresenter(validationManager, resources)
        mainPresenter.attachView(mainView)
    }

    @Test
    fun attachView_callsSetDemoXpub() {
        verify(mainView).setDemoXpub(ArgumentMatchers.anyString())
    }

    @Test
    fun onXPubEntered_whenAddressIsValid_loadsWallet() {
        mainPresenter.onXPubEntered(MainPresenter.DEMO_XPUB)
        verify(mainView).loadWallet(MainPresenter.DEMO_XPUB)
    }

    @Test
    fun onXPubEntered_whenAddressIsNotValid_showsErrorMessage() {
        Mockito.`when`(resources.getString(ArgumentMatchers.anyInt())).thenReturn("")
        mainPresenter.onXPubEntered("")
        verify(mainView).showErrorMessage(ArgumentMatchers.anyString())
    }
}